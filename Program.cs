﻿using System;
using System.IO;
using System.Linq;


namespace CreateFile
{   
    class Program
    {
          static void Main(string[] args)
          {                        
             string str;
             string fileName = "text.txt";       
              
             using (StreamWriter sw = new StreamWriter(fileName))
             {
                var rand = new Random();
                for (int i = 0; i < 100; i++)                
                    sw.Write($"{rand.Next(21)},");                
             }

             using (StreamReader sr = new StreamReader(fileName))
             {
                str = sr.ReadLine();
             }
             var result = str.Split(',').
                                      Where(x => !string.IsNullOrWhiteSpace(x)).
                                      Select(x => int.Parse(x)).ToArray();

             Array.Sort(result);
             foreach (var e in result)
                Console.Write($"{e} ");           

             Array.Reverse(result);
           
                Console.ReadLine();
          }
    }   

}
   

